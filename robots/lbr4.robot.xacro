<?xml version="1.0" ?>
<robot xmlns:xacro="http://www.ros.org/wiki/xacro" name="lbr4">

  <xacro:arg name="end_effector" default="None"  doc="[allegro, reflex, push_stick]"/>
  <xacro:arg name="gazebo"       default="true"  />
  <xacro:arg name="ft_sensor"    default="false" />
  <xacro:arg name="omni"         default="false" />
  <xacro:arg name="robot_table"  default="false" />
  <xacro:arg name="robot_name"   default="lbr4"  />
  <xacro:arg name="control"      default="false" />
  <xacro:arg name="biotac"       default="false" />

  <!-- ========================================================================================== -->
  <!-- Convert arg to property for evaluation of string equality. TODO ideally this would just    -->
  <!-- work by directly evaluating using the args but Adam couldn't get it to work.               -->
  <xacro:property name="end_effector" value="$(arg end_effector)"/>
  <xacro:property name="M_PI"         value="3.1415926535897931"/>
  <xacro:property name="urdf_path"    value="$(find ll4ma_robots_description)/urdf"/>

  <!-- ========================================================================================== -->
  
  <!-- LBR4 -->
  <xacro:include filename="${urdf_path}/lbr4/lbr4.urdf.xacro"/>
  <xacro:include filename="${urdf_path}/lbr4/lbr4_base.urdf.xacro"/>
  <xacro:lbr4_base/>
  <xacro:lbr4 name="lbr4" parent="base_link">
    <origin xyz="0.0015 -0.0015 0.009" rpy="0 0 0"/>
  </xacro:lbr4>

  <!-- World -->
  <link name="world" />
  <!-- World to base (if no table) -->
  <xacro:unless value="$(arg robot_table)">
    <joint name="world_to_base_joint" type="fixed">
      <origin xyz="0 0 0.01" rpy="0 0 0"/>
      <parent link="world"/>
      <child link="base_link"/>
    </joint>
  </xacro:unless>
  
  <!-- Gazebo -->
  <xacro:if value="$(arg gazebo)">
    <xacro:include filename="${urdf_path}/lbr4/lbr4.gazebo.xacro">
      <xacro:arg name="ft_sensor"  value="$(arg ft_sensor)"/>
      <xacro:arg name="robot_name" value="$(arg robot_name)"/>
    </xacro:include>
  </xacro:if>

  <!-- Allegro hand -->
  <xacro:if value="${end_effector == 'allegro'}">
    <xacro:include filename="$(find ll4ma_robots_description)/robots/allegro.robot.xacro">
      <xacro:arg name="gazebo" value="$(arg gazebo)"/>
      <xacro:arg name="control" value="$(arg control)"/>
      <xacro:arg name="biotac" value="$(arg biotac)"/>
    </xacro:include>
    
    <!-- Include the KUKA mount -->
    <xacro:include
        filename="${urdf_path}/mounts/lbr4_mount.urdf.xacro"/>
    <xacro:lbr4_mount parent="lbr4_7_link"/>
    
    <joint name="lbr4_allegro" type="fixed">
      <parent link="lbr4_mount"/>
      <child link="allegro_mount"/>
      <origin xyz="-0.0425 0 -0.0425" rpy="0 1.57 0"/>
    </joint>
  </xacro:if>

  <!-- ReFlex Hand -->
  <xacro:if value="${end_effector == 'reflex'}">
    <xacro:include filename="${urdf_path}/reflex/reflex.urdf.xacro"/>
    <!-- Include the KUKA mount -->
    <xacro:include filename="${urdf_path}/mounts/lbr4_mount.urdf.xacro"/>
    <xacro:lbr4_mount parent="lbr4_7_link"/>

    <!-- Include the ReFlex mount -->
    <xacro:include filename="${urdf_path}/mounts/lbr4_reflex_mount.urdf.xacro"/>

    <joint name="reflex_mount_to_lbr4_mount" type="fixed">
      <parent link="lbr4_mount"/>
      <child link="reflex_mount"/>
      <origin xyz="-0.003 -0.002 0" rpy="0 ${M_PI/2} 0.2"/>
    </joint>
    
    <joint name="reflex_mount_to_reflex" type="fixed">
      <parent link="reflex_mount"/>
      <child link="lower_shell"/>
      <origin xyz="0.013 0.0025 0" rpy="${M_PI} ${-M_PI/2} 0"/>
    </joint>

    <xacro:if value="$(arg gazebo)">
      <xacro:include filename="$(find ll4ma_robots_description)/urdf/reflex/reflex.gazebo.xacro"/>
    </xacro:if>
  </xacro:if>

  <!-- Force/Torque sensor -->
  <xacro:if value="$(arg ft_sensor)">
    <xacro:include filename="${urdf_path}/force_sensors/optoforce_sensor.urdf.xacro"/>
    
    <joint name="optoforce_to_lbr4" type="fixed">
      <parent link="lbr4_7_link"/>
      <child link="optoforce_model_link"/>
      <origin xyz="0 0 0" rpy="0 0 0"/>
    </joint>

    <!-- Push-stick -->
    <xacro:if value="${end_effector == 'push_stick'}">
      <xacro:include filename="${urdf_path}/end_effectors/push_stick.urdf.xacro"/>
      <xacro:include filename="${urdf_path}/mounts/optoforce_push_stick_mount.urdf.xacro"/>
      
      <xacro:optoforce_push_stick_mount/>
      <xacro:push_stick/>

      <joint name="optoforce_to_push_stick_mount" type="fixed">
	<xacro:if value="$(arg gazebo)">
	  <origin xyz="0 0 0" rpy="0 0 0"/>
	  <parent link="optoforce_sensor_link"/>
	</xacro:if>
	<xacro:unless value="$(arg gazebo)">
	  <origin xyz="0 0 0.035" rpy="0 0 0"/>
	  <parent link="optoforce_model_link"/>
	</xacro:unless>

	<child link="optoforce_push_stick_mount_link" />
      </joint>
      
      <joint name="optoforce_mount_to_push_stick" type="fixed">
	<origin xyz="0 0 0.01" rpy="0 0 0" />
	<parent link="optoforce_push_stick_mount_link" />
	<child link="push_stick_link" />
      </joint>
    </xacro:if>

    <xacro:if value="${end_effector == 'cube'}">
      <xacro:include filename="{urdf_path}/end_effectors/cube.urdf.xacro"/>
      <xacro:cube_ee parent="optoforce_sensor_link"/>
    </xacro:if>
  </xacro:if>

  <!-- Table -->
  <xacro:if value="$(arg robot_table)">
    <xacro:include
        filename="${urdf_path}/environment/robot_table.urdf.xacro"/>
    <joint name="world_to_table_joint" type="fixed">
      <origin xyz="0 0 0.295" rpy="0 0 0"/>
      <parent link="world"/>
      <child link="table_link"/>
    </joint>
    <joint name="table_to_base_joint" type="fixed">
      <origin xyz="0 0 0.305" rpy="0 0 1.57" />
      <parent link="table_link" />
      <child link="base_link" />
    </joint>

    <!-- Omni -->
    <xacro:if value="$(arg omni)">
      <xacro:include filename="${urdf_path}/phantom_omni/omni.urdf.xacro"/>
      <joint name="table_to_omni_joint" type="fixed">
	<!-- <origin xyz="-0.35 0.3 0" rpy="0 0 0" /> -->
	<!-- <parent link="table_link" /> -->
	<origin xyz="0 0 0" rpy="0 0 0" />
	<parent link="world" />	
	<child link="base" />
      </joint>
    </xacro:if>
  </xacro:if>
  
</robot>
